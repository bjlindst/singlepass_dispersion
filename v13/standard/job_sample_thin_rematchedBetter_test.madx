option, warn,info;
system,"ln -fns /afs/cern.ch/eng/lhc/optics/runIII lhc";
system,"ln -fns /afs/cern.ch/eng/lhc/optics/HLLHCV1.3  slhc";

option, -echo, -warn,info;
call,file="slhc/toolkit/macro.madx";
call,file="slhc/aperture/const_for_aperture.madx";
call,file="lhc/lhc.seq";

mylhcbeam = 1;
exec,mk_beam(7000);

call,file="slhc/hllhc_sequence.madx";
call,file="lhc/aperture/aperture.b1.madx";
call,file="lhc/aperture/aperture.b2.madx";
call,file="lhc/aperture/aper_tol.b1.madx";
call,file="lhc/aperture/aper_tol.b2.madx";
call,file="slhc/aperture/exp_pipe_model_after_LS3.madx";
call,file="slhc/aperture/exp_pipe_install_after_LS3.madx";
call,file="slhc/aperture/aperture_upgrade_IT.madx";
call,file="slhc/aperture/aperture_upgrade_MS.madx";   
seqedit,sequence=lhcb1;flatten;cycle,start=IP1;endedit;
seqedit,sequence=lhcb2;flatten;cycle,start=IP1;endedit;      
call, file = "patchApertures.madx";
call, file = "fromCollimation_define.madx";
call, file = "fromCollimation_install.madx";  

!!! change quadrupole strengths to unique variables for matching
MQ.7L7.B1,    K1 := my1, polarity=-1;
MQ.8L7.B1,    K1 := my2, polarity=+1;
MQ.9L7.B1,    K1 := my3, polarity=-1;
MQ.10L7.B1,   K1 := my4, polarity=+1;
MQWA.E5L7.B1, K1 := my5, polarity=-1;
MQWA.D5L7.B1, K1 := my6, polarity=-1;
MQWA.C5L7.B1, K1 := my7, polarity=-1;
MQWA.B5L7.B1, K1 := my8, polarity=-1; 
MQWA.A5L7.B1, K1 := my9, polarity=-1; 
MCBWH.4L7.B1, KICK := my10;    
my1 := kqd.a67; 
my2 := kqf.a67;
my3 := kqd.a67;
my4 := kqf.a67;
my5 := kq5.lr7;
my6 := kq5.lr7;
my7 := kq5.lr7;
my8 := kq5.lr7; 
my9 := kq5.lr7;  
my10 := 0;

mbh_shift=0; ! must set this to zero when not using MBH otherwise aperture will be wrong, this shift is due to MBH being RBEND   
make_end_markers=1;   
exec,myslice;

call,file="slhc/opt_150_150_150_150_thin.madx";

set,format="14.8f";
select, flag=twiss, CLEAR; 
select, flag=twiss,column=name,s,l,mux,betx,alfx,muy,bety,alfy,dx,dy,x,y,px,py,apertype;   

on_x1:=250; on_x2:=200; on_x5:=250; on_x8:=170; ! xing angles [murad]
phi_IR1:=0; phi_IR2:=90; phi_IR5:=90; phi_IR8:=90; ! xing plane      
on_sep1:=0; on_sep2:=0; on_sep5:=0; on_sep8:=0; ! beam separations at xing point [mm]
on_sol_atlas:=0; on_sol_alice:=0; on_sol_cms:=0; ! solenoid switches [0/1]
on_alice:=7000/nrj; on_lhcb:=7000/nrj;           ! spectrometer dipoles in ALICE and LHCb [0,1; 1 is for 7 TeV beam]
on_a1=0; on_a2=0; on_a5=0; on_a8=0;              ! add crossing angle in the non-nominal plane as well, keep at 0 [murad]
on_o1=0; on_o2=0; on_o5=0; on_o8=0;             ! add offset - plane depends on phi_IRx
on_crab1:=0; on_crab5:=0; ! crab cavity switches [0/1]
on_disp=1;       ! switch for arc orbit correctors, leave at 1 [0/1]   

call, file="/afs/cern.ch/eng/lhc/optics/HLLHCV1.3/toolkit/rematch_chroma.madx";
call, file="/afs/cern.ch/eng/lhc/optics/HLLHCV1.3/toolkit/rematch_tune.madx";


savebeta, label=entr,place=MQ.10L7.B1;
savebeta, label=extr,place=MQWA.A5L7.B1;  
select, flag=twiss, CLEAR; 
select, flag=twiss,column=name,s,mux,betx,alfx,muy,bety,alfy,dx,dy,x,y,px,py,apertype;  
use,sequence=lhcb1;twiss, table=twiss;!write,table=twiss,file="out/LHC_b1_7000GeV_thinXing.twiss";  


use,sequence=lhcb1;
match, sequence=lhcb1;
    vary, name=my10;
    !constraint,sequence=lhcb1,range=TCP.C6L7.B1,betx=140.6830743,alfx=1.83961914,bety=75.42429451,alfy=-1.01271608;
    !constraint,sequence=lhcb1,range=MQ.10L7.B1,betx=entr->betx,bety=entr->bety,alfx=entr->alfx,alfy=entr->alfy;  
    constraint,sequence=lhcb1,range=MQWA.A5R7.B1,dx=2;  
    lmdif,calls=500,tolerance=1e-20;
endmatch;      
!call, file="/afs/cern.ch/eng/lhc/optics/HLLHCV1.3/toolkit/rematch_tune.madx";  

seqedit, sequence=lhcb1;flatten;cycle,start=TCP.C6L7.B1;endedit;
select, flag=twiss, CLEAR; 
select, flag=twiss,column=name,keyword,s,l,mux,betx,alfx,muy,bety,alfy,dx,dy,x,y,px,py,apertype;   
use,sequence=lhcb1;twiss, centre=true, table=twiss;write,table=twiss,file="out/LHC_b1_7000GeV_thinXing_matchedBetter.twiss";  


betxTCP=table(twiss,TCP.C6L7.B1,betx);
alfxTCP=table(twiss,TCP.C6L7.B1,alfx);
betyTCP=table(twiss,TCP.C6L7.B1,bety);
alfyTCP=table(twiss,TCP.C6L7.B1,alfy);

select, flag=twiss, column=KEYWORD,NAME,S,L,X,Y,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,PX,PY;
twiss,  betx=betxTCP, bety=betyTCP,  alfx=alfxTCP ,alfy=alfyTCP, dx=0, dy=0, dpx=0,dpy=0, sequence=lhcb1, file="out/twiss_b1_singlepass_matchedBetter.twiss", save;


system, "rm lhc slhc d2_aperture";     
