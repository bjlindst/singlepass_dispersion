option, warn,info;
system,"ln -fns /afs/cern.ch/eng/lhc/optics/runIII lhc";
system,"ln -fns /afs/cern.ch/eng/lhc/optics/HLLHCV1.5  slhc";
system,"ln -fns ../../../generate_aperture/extraTools myScripts"; ! link to aperture offset script
system,"ln -fns ../../../generate_aperture/processedFiles processedFiles"; ! link to layoutDB and aperture patching files, from generate_aperture scripts
option, -echo, -warn,info;
call,file="slhc/toolkit/macro.madx";
call,file="slhc/aperture/const_for_aperture.madx";
call,file="lhc/LHC_LS2_2021-07-02.seq";

!--------------------------------------------------
! OPTIONS
!--------------------------------------------------  

betaStar=15; ! cm
!betaStar=20;
!betaStar=64;
!betastar=100;

!-------------------------------------------------
!
!------------------------------------------------- 

mylhcbeam = 1;
exec,mk_beam(7000);
call,file="slhc/hllhc_sequence.madx";
call,file="lhc/aperture/aperture.b1.madx";
call,file="lhc/aperture/aperture.b2.madx";
call,file="lhc/aperture/aper_tol.b1.madx";
call,file="lhc/aperture/aper_tol.b2.madx";
call,file="slhc/aperture/exp_pipe_model_after_LS3.madx";
call,file="slhc/aperture/exp_pipe_install_after_LS3.madx";
call,file="slhc/aperture/aperture_upgrade_IT.madx";
call,file="slhc/aperture/aperture_upgrade_MS.madx";
call,file="processedFiles/layoutDB.seq";
call,file="processedFiles/patchApertures_b1_release_v06.madx";
call,file="processedFiles/fromCollimation_define.madx";
call,file="processedFiles/fromCollimation_install.madx";   
seqedit,sequence=lhcb1;flatten;endedit;
seqedit,sequence=lhcb2;flatten;endedit;
call,file="processedFiles/correctMechSep3.madx"; 
mbh_shift=0; ! must set this to zero when not using MBH otherwise aperture will be wrong, this shift is due to MBH being RBEND
make_end_markers=1;   
exec,myslice;   
  
if(betaStar==15){call,file="slhc/round/opt_round_150_1500_thin.madx";}
elseif(betaStar==20){call,file="slhc/round/opt_round_200_1500_tcdq4_thin.madx";}
elseif(betaStar==64){call,file="slhc/ramp/opt_ramp_640_1500_thin.madx";}
elseif(betaStar==100){call,file="slhc/ramp/opt_ramp_1000_1500_thin.madx";}  

! Set the xing knobs
call,file="auxSettings/xingKnobs.madx";

! Rematch post-knobs
call, file="slhc/toolkit/rematch_chroma.madx";
call, file="slhc/toolkit/rematch_tune.madx";   
 
seqedit, sequence=lhcb1;flatten;cycle,start=TCP.C6L7.B1;endedit;
select, flag=twiss, CLEAR;
set,format="14.8f";
select, flag=twiss, column=NAME,S,X,Y,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,PX,PY;  
use,sequence=lhcb1;twiss, centre=true, table=twiss;write,table=twiss,file="out/LHC_b1_7000GeV_thinXing.twiss";   

betxTCP=table(twiss,TCP.C6L7.B1,betx);
alfxTCP=table(twiss,TCP.C6L7.B1,alfx);
betyTCP=table(twiss,TCP.C6L7.B1,bety);
alfyTCP=table(twiss,TCP.C6L7.B1,alfy);

select, flag=twiss, column=NAME,S,X,Y,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,PX,PY;
twiss, centre=true, betx=betxTCP, bety=betyTCP,  alfx=alfxTCP ,alfy=alfyTCP, dx=0, dy=0, dpx=0,dpy=0, sequence=lhcb1, file="out/twiss_b1_singlepass.twiss", save;   

value, MCBWH.4L7.B1->kick;
acbwh4.l7b1 = 0.3*2.44*0.904*299792458/7e12;
value, MCBWH.4L7.B1->kick;
value, MCBWH.5R7.B1->kick;
value, MCBCH.7R7.B1->kick;
value, MCBCH.9R7.B1->kick;

use, sequence=lhcb1;
match, sequence=lhcb1;
    !vary, name=acbwh5.r7b1;
    vary, name=acbch7.r7b1;
    vary, name=acbch9.r7b1;
    !constraint,sequence=lhcb1,range=MCBWH.5R7.B1,px=0;
    constraint,sequence=lhcb1,range=MCBCH.9R7.B1,x=0,px=0;
    lmdif,calls=500,tolerance=1e-20;
endmatch;
value, MCBWH.4L7.B1->kick;
value, MCBWH.5R7.B1->kick;
value, MCBCH.7R7.B1->kick;
value, MCBCH.9R7.B1->kick;   
use,sequence=lhcb1;twiss, centre=true, table=twiss;write,table=twiss,file="out/LHC_b1_7000GeV_thinXing_afterCorr.twiss";  

betxTCP=table(twiss,TCP.C6L7.B1,betx);
alfxTCP=table(twiss,TCP.C6L7.B1,alfx);
betyTCP=table(twiss,TCP.C6L7.B1,bety);
alfyTCP=table(twiss,TCP.C6L7.B1,alfy);

twiss, centre=true, betx=betxTCP, bety=betyTCP,  alfx=alfxTCP ,alfy=alfyTCP, dx=0, dy=0, dpx=0,dpy=0, sequence=lhcb1, file="out/twiss_b1_afterCorr_singlepass.twiss", save;
 
system, "rm lhc slhc myScripts processedFiles d2_aperture";     
